# Content of test_class.py
# 'python -m pytest' to run all test cases
# Must install pytest

from SwarmObjects import *
from DroneController import *

class TestController:
    '''
    TEST ONE: Checks to see if all objects in droneArray are drone objects and each drone was properly indexed.
    '''
    def test_one(self):
        drones = []
        control = DroneController(800, 800, drones, 0 , 0)
        numDronesAvailable = 20
        for i in range(numDronesAvailable):
            drones.append(Drone(0, 0, 15, 0, None, None, 30, i))
        control.assignDronePositions(200, 200, math.pi * (59**2))
        counter = 0
        for drone in control.droneArray:
            assert type(drone) == Drone  # Check that all values in DroneArray are Drone objects
            assert drone.droneNumber == counter  # Check that each drone was properly initiated with its unique number
            counter += 1
    '''
    TEST TWO: Iterates from Layer 0 to Layer 7, checking that at each layer, the proper number of drones is allocated
    depending on the radii given. The radii were specifically calculated to produce areas at critical areas.
    '''
    def test_two(self):
        # Number of drones, layers required
        layers = [1]
        for i in range(7):
            drones = 6 * (i + 1) + layers[i]
            # print(drones)
            layers.append(drones)

        # Test for layers 0-7 that the controller allocates enough layers depending on number of drones required
        radiiToTest = [10, 36, 59, 82, 106, 130, 153, 177]
        for i in range(8):
            # print("Layer ", i)
            drones = []
            numDronesAvailable = layers[i]
            for x in range(numDronesAvailable):
                # print("Drone ", x, " appended.")
                drones.append(Drone())
            control = DroneController(800, 800, drones, 0, 0)
            control.assignDronePositions(200, 200, math.pi * (radiiToTest[i] ** 2))
            counter = 0
            for drone in control.droneArray:
                if drone.xloc != 0 and drone.yloc != 0:
                    counter += 1
            # print(counter, layers[i])
            assert counter == layers[i]

    '''
    TEST THREE: tests to see if the system correctly detects if a battery for a drone is low.
    A low battery is if the minutes remaining is < 15. 
    '''
    def test_three(self):
        drones = []
        for i in range(10):
            drones.append(Drone(radius=15, batteryPercent=100, droneNumber=i, xloc=0, yloc=0))
            if i == 6:
                drones.append(Drone(radius=15, batteryPercent=12, droneNumber=i, xloc=0, yloc=0))

        control = DroneController(800, 1600, drones, 0, 0)
        assert control.checkForErrors() == "Low battery"


    '''
    TEST FOUR: Check that a drone successfully initializes.
    '''
    def test_four(self):
        testDrone = Drone()
        # xloc = 0, yloc = 0, radius = 15, altitude = 0, droneType = None, frequency = None, batteryPercent = None, droneNumber = -1
        assert testDrone.xloc == 0
        assert testDrone.yloc == 0
        assert testDrone.droneType == None
        assert testDrone.frequency == None
        assert testDrone.radius == 15
        assert testDrone.altitude == 0
        assert testDrone.areaCircle == math.pi * (testDrone.radius ** 2)
        # Two times the formula of a trapezoid = (b1+b2)(h) or (3/2)sqrt(3)(r^2)
        assert testDrone.areaHexagon == (3 * math.sqrt(3) * math.pow(testDrone.radius, 2)) / 2
        assert testDrone.batteryPercent == None
        assert testDrone.droneNumber == -1

    '''
    TEST FIVE: Tests that a drone's battery drone condition is properly checked.
    '''
    def test_five(self):
        greenDrone = Drone()
        yellowDrone = Drone()
        redDrone = Drone()
        greenDrone.batteryPercent = 90
        yellowDrone.batteryPercent = 48
        redDrone.batteryPercent = 3

        assert greenDrone.batteryCondition() == 'green'  # Battery is green
        assert yellowDrone.batteryCondition() == 'yellow'  # Battery is yellow
        assert redDrone.batteryCondition() == 'tomato'  # Battery is low

        # Change battery percentages
        greenDrone.batteryPercent = 1
        yellowDrone.batteryPercent = 90
        redDrone.batteryPercent = 44

        assert greenDrone.batteryCondition() == 'tomato'
        assert yellowDrone.batteryCondition() == 'green'
        assert redDrone.batteryCondition() == 'yellow'

    '''
    TEST SIX: Tests that the controller properly replaces a drone.
    '''
    def test_six(self):
        # Create a controller with drones 0-18 being used. The controller will then replace drone 0 with drone 19
        drones = []
        control = DroneController(800, 800, drones, 0, 0)
        numDronesAvailable = 20
        for i in range(numDronesAvailable):
            drones.append(Drone(0, 0, 15, 0, None, None, 30, i))
        control.assignDronePositions(200, 200, math.pi * (59 ** 2))
        xloc = control.droneArray[0].xloc
        yloc = control.droneArray[0].yloc
        control.replaceDrone(control.droneArray[0])

        # Check that the first drone not initially used is now being used
        assert xloc == control.droneArray[19].xloc
        assert yloc == control.droneArray[19].yloc
        assert control.droneArray[19].deployed == True

    '''
    TEST SEVEN: Tests that a waypoint is properly created.
    '''
    def test_seven(self):
        testWaypoint = Waypoint(10, 10, 100)
        # Check waypoint attributes were properly initialized
        assert testWaypoint.xloc == 10
        assert testWaypoint.yloc == 10
        assert testWaypoint.altitude == 100

    '''
    TEST EIGHT: Tests that a waypoint is properly assigned to a drone.
    '''
    def test_eight(self):
        drone = Drone()
        waypoint = Waypoint(0, 0, 100)
        drone.assignWaypoint(waypoint)
        assert drone.waypoint != None
        assert drone.waypoint.xloc == 0
        assert drone.waypoint.yloc == 0
        assert drone.waypoint.altitude == 100

    '''
    TEST NINE: Tests that a replacement is properly found.
    '''
    def test_nine(self):
        drone_list = []
        for i in range(5):
            drone_list.append(Drone())
        control = DroneController(400, 400, drone_list, 0, 0)
        # All drones should not be deployed. Check that they aren't, then deploy them
        for drone in control.droneArray:
            assert drone.deployed == False
            drone.deployed = True
        assert control.findReplacement() == -1  # Return -1 if all drone are deployed
        # Undeploy drone 2
        control.droneArray[2].deployed = False
        assert control.findReplacement() == 2   # Return 2 since drone 2 is not deployed

    """
    TEST TEN: Tests that a drone is properly moved by the drone controller.
    """
    def test_ten(self):
        drone = Drone()
        drone_list = []
        control = DroneController(400, 400, drone_list, 0, 0)
        control.droneArray.append(drone)
        # Adjust drone speed and heading so it moves from (0,0) to (5,5)
        control.droneArray[0].waypoint = Waypoint(5, 5, -1)
        control.droneArray[0].heading = control.droneArray[0].getHeading()
        # control.droneArray[0].heading = (45 * math.pi ) / 180
        control.droneArray[0].speed = math.sqrt(math.pow(5, 2) + math.pow(5, 2))
        # Move drone and check that it's now at (5,5)
        control.moveDrone(control.droneArray[0])
        assert int(control.droneArray[0].xloc) == 5
        assert int(control.droneArray[0].yloc) == 5

    """
    TEST 11: Tests that the controller is properly updated.
    """
    def test_eleven(self):
        drone = Drone()
        drone_list = []
        control = DroneController(400, 400, drone_list, 0, 0)
        control.droneArray.append(drone)
        control.droneArray[0].deployed = True
        # Adjust drone so that it will be able to move to (1,1)
        control.droneArray[0].waypoint = Waypoint(1, 1, -1)
        control.droneArray[0].heading = control.droneArray[0].getHeading()
        # control.droneArray[0].heading = (45 * math.pi) / 180
        control.droneArray[0].speed = math.sqrt(2)
        # Update drone controller then check to see if updated properly
        control.updateController()
        assert int(control.droneArray[0].xloc) == 1
        assert int(control.droneArray[0].yloc) == 1
        # assert control.weather != None

    """
    TEST 12: Tests that a drone is properly initialized.
    """
    def test_twelve(self):
        drone = Drone(xloc=0, yloc=0, radius=15, altitude=0, droneType=None, frequency=None, batteryPercent=90,
                      droneNumber=-1, speed=5)
        assert drone.xloc == 0
        assert drone.yloc == 0
        assert drone.droneType == None
        assert drone.frequency == None
        assert drone.radius == 15
        assert drone.altitude == 0
        # A= pi*r^2
        assert drone.areaCircle == math.pi * (drone.radius ** 2)
        # Two times the formula of a trapezoid == (b1+b2)(h) or (3/2)sqrt(3)(r^2)
        assert drone.areaHexagon == (3 * math.sqrt(3) * math.pow(drone.radius, 2)) / 2
        assert drone.batteryPercent == 90
        assert drone.droneNumber == -1
        assert drone.deployed == False
        assert drone.speed == 5
        assert drone.heading == 0
        assert drone.waypoint == None

    """
    TEST 13: Tests if a drone is properly killed.
    """
    def test_thirteen(self):
        drones = []
        control = DroneController(800, 800, drones, 0, 0)
        numDronesAvailable = 20
        for i in range(numDronesAvailable):
            drones.append(Drone(0, 0, 15, 0, None, None, 30, i))
        control.assignDronePositions(200, 200, math.pi * (59 ** 2))
        control.killDrone(drones[0])

        assert drones[0].xloc == -1
        assert drones[0].yloc == -1
        assert drones[0].deployed == False
        assert drones[0].waypoint == None