import math
from Battery import *

class Drone:
    def __init__(self, xloc, yloc, deployed, waypoint, currentBattery, speed=10):
        self.xloc = xloc
        self.yloc = yloc
        self.deployed = deployed
        self.waypoint = waypoint
        self.heading = 0
        self.currentBattery = currentBattery
        self.number = -1
        self.speed = speed

    def assignWaypoint(self, waypoint):
        """
        Assigns new waypoint to drone and gets heading based off of its new waypoint.
        :param waypoint: specific Waypoint object for the drone to travel to
        :return: None
        """
        self.waypoint = waypoint

    def assignHeading(self):
        """
        Gets heading between current location and new Waypoint
        :return: None
        """
        if self.waypoint == None:
            # print("NO heading")
            return None
        else:
            dy = self.waypoint.yloc - self.yloc
            dx = self.waypoint.xloc - self.xloc

            heading = math.atan2(dx, dy)

            self.heading = heading

    def updateLocation(self):
        """
        Updates the x and y location of the drones
        :return: None
        """

        # If the drone is at the assigned waypoint, stop it and assign its position to the waypoint
        if self.destReached():
            self.xloc = self.waypoint.xloc
            self.yloc = self.waypoint.yloc

        #Otherwise, update the drone's heading and position based on its speed
        else:
            tempx = self.xloc
            tempy = self.yloc
            self.assignHeading()
            self.xloc += self.speed * math.sin(self.heading)
            self.yloc += self.speed * math.cos(self.heading)

    def destReached(self):
        return abs(self.xloc - self.waypoint.xloc) < 5 and abs(self.yloc - self.waypoint.yloc) < 5

    def initializeBatteries(self):
        """
        Adds these three batteries to the battery array for the selected drone. Batteries should be fully charged.
        :return: None
        """
        battery1 = Battery(100, 0)
        battery2 = Battery(100, 1)
        battery3 = Battery(100, 2)
        self.assignedBatteries = [battery1, battery2, battery3]

    def rechargeBattery(self):
        """
        Recharges the drones battery if it is not being used
        :return: None
        """
        for battery in self.assignedBatteries:
            if battery is not self.currentBattery:
                battery.rechargeBattery()

    def drainBattery(self):
        """
        Drains the battery that is currently being used
        :return: None
        """
        self.currentBattery.drainBattery()



