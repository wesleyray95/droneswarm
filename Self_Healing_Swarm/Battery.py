"""
A Battery object that will be attached to a drone.The battery has a life percentage, and rates for how fast it charges and drains.
"""

class Battery:

    def __init__(self, batteryPercentage, batteryIndex, rechargeRate=0.5, drainRate=0.1):
        self.batteryPercentage = batteryPercentage
        self.batteryIndex = batteryIndex
        self.assignedDrone = None
        self.rechargeRate = rechargeRate
        self.drainRate = drainRate
        self.L = 100

    def __str__(self):
        return "Battery Percentage = {}\nBattery Index = {}\n".format(self.batteryPercentage, self.batteryIndex)

    def drainBattery(self):
        """
        Set new percentage by subtracting battery's drain rate from current percentage
        :return: None
        """
        self.batteryPercentage -= self.drainRate
        if self.batteryPercentage <= 0:
            print("ERROR: DRONE DIED")

    def rechargeBattery(self):
        """
        Adds the recharge rate to the battery's current percentage
        :return: None
        """
        self.batteryPercentage += self.rechargeRate

    def batteryConditionColor(self):
        """
        Check battery condition and return the color for the drone to be used by the GUI
        :return: 'green' for full battery, 'yellow' for half, and 'red' for low battery
        """
        halfBatteryThreshold = 50
        lowBatteryThreshold = 10

        bat_ratio = self.batteryPercentage * .01

        red_val = abs(int((1 - bat_ratio) * 255))
        red_val = min(255, red_val)
        grn_val = abs(int(bat_ratio * 255))
        grn_val = min(255, grn_val)

        return "#%02x%02x%02x" % (red_val, grn_val, 0)
        # if self.batteryPercentage > halfBatteryThreshold:
        #     return "#000000"
        # elif self.batteryPercentage > lowBatteryThreshold:
        #     return 'yellow'
        # else:
        #     return 'tomato'