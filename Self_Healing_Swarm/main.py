from math import cos
from math import sin
from math import atan2
import math
import time

from GUI import *
from tkinter import *
from DroneController import *
from Drone import *
from Battery import *
from Waypoint import *


def convert_to_rad(angle):
    return angle * math.pi / 180


def convert_gps(x, y):

    R = 6371000
    d_lon = convert_to_rad(y[1] - x[1])
    d_lat = convert_to_rad(y[0] - x[0])

    a = sin(d_lat/2)**2 + cos(x[0]) * cos(y[0]) * sin(d_lon/2)**2
    c = 2 * atan2(math.sqrt(a), math.sqrt(1 - a))
    d = R * c
    return d


def main():
    controller = DroneController(30, 30000, 500, 300)
    controller.initializeDroneController()

    root = Tk()
    myGUI = GUI(root)

    myGUI.drawBackground(myGUI.img)
    myGUI.drawDroneInfo(controller.drones)

    # update GUIs
    myGUI.updateGUI(controller)

    # Update GUI
    root.update()
    root.update_idletasks()

    interval = 0.0167    # Delay between updates
    # Update the GUI and controller at each timer interval (currently every .25 seconds)
    frame = 0
    while True:
        time.sleep(interval - time.monotonic() % interval)
        controller.updateController()
        myGUI.updateGUI(controller)
        root.update()
        root.update_idletasks()
        frame += 1

if __name__ == "__main__":
    main()
