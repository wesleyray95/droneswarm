"""
  A distinct point for a drone to move to.
"""
#TODO : add altitude when we implment 3D model

class Waypoint:
    def __init__(self, xloc, yloc):
        self.xloc = xloc
        self.yloc = yloc
        self.dronesAssigned = []
