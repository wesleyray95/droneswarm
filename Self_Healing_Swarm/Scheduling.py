import math
# from DroneController import *
from math import sin, cos, atan2
# from geopy.distance import geodesic

class Scheduling:
    """
    Creates disjoint subsets of waypoints and returns the number of spare
    drones needed; Follows the findings found in the following paper:
    Scheduling Spare Drones for Persistent Task Performance under Energy Constraints;

    We will be implementing the offline version of BMIDP Problem.

    home_loc = location of home station
    b_capacity = battery capacity
    b_discharge = battery discharge rate
    drone_speed = drone speed (all homogeneous drones) (ft/s)
    locations = list of waypoints
    """
    def __init__(self, home_loc, b_capacity, b_discharge, drone_speed, locations=[], test_env=False):
        self.home_loc = home_loc
        self.L = b_capacity
        self.c = b_discharge
        self.drone_speed = drone_speed
        self.spare_drones = 1
        self.subsets = []
        self.test_env = test_env
        self.locations = self.create_loc_dict(locations)
        self.locations.sort(key=lambda x: x[1], reverse=True)
        self.find_spare_drones()

    def create_loc_dict(self, locations):
        temp_locations = []
        for x in range(len(locations)):
            # print(x)
            value = self.loc_value(self.c, self.calc_t(locations[x], self.test_env))
            temp_locations.append([locations[x], value])

        # print("Original dictionary", temp_locations)

        return temp_locations

    @staticmethod
    def sort_decr_order(array, index=0):
        temp = array.sort(key=lambda x: x[1], reverse=True)
        return temp

    """
    Returns the time it takes to return to home station.
    """
    def calc_t(self, loc, test_env):
        if self.test_env:
            return math.sqrt(loc.xloc**2 + loc.yloc **2) / self.drone_speed
        # return geodesic(self.home_loc, loc).meters / self.drone_speed

    """
    Returns the max flight time (in seconds) from all waypoints to home station.
    """
    def calc_t_max(self):
        max_time = 0
        for loc in self.locations:
            time = self.calc_t(loc[0], 1)
            if time > max_time:
                max_time = time

        return max_time

    @staticmethod
    def loc_value(discharge_rate, time_to_home):
        return 2 * discharge_rate * time_to_home

    def check_for_single_spare_drone(self):
        """
        Requirement 1 must be satisfied in order for a single spare drone to properly
        replace all drones.
        Req 1: L - 2c*t_max >= for all items: sum(2*c*t)

        t: the time it takes for the drone to travel back to home station.
        t_max: the max time it takes for any drone to travel back to home station.
        loc_value: 2 * (battery discharge rate) * (time to travel back to home station)
        :return: boolean value for requirement 1
        """
        max_item_value = self.locations[0][1]

        # L - 2*c*t_max
        left_side = self.L - max_item_value

        # Sum of all loc values
        right_side = 0
        for loc in self.locations:
            right_side += loc[1]

        return left_side >= right_side

    # @staticmethod
    # def swap(array, x, y):
    #     temp = array[x]
    #     array[x] = array[y]
    #     array[y] = temp

    def create_disjoint_loc_subsets(self):
        """
        If requirement 1 is not satisfied, we must split the locations into disjoint
        subsets that can each be properly replaced by one spare drone for each subset;
        uses greedy algorithm for creating subsets.
        :return:
        """

        x = []
        self.subsets.append(x)
        for loc in self.locations:
            did_it_fit = False
            maximum = 0
            sum = loc[1]
            for subset in self.subsets:
                for sub_loc in subset:
                    maximum = max(maximum, sub_loc[1])
                    sum += sub_loc[1]
                sum += maximum
                if sum <= self.L:
                    did_it_fit = True
                    subset.append(loc)
                if did_it_fit:
                    break  # location was put into a subset; next location; stop traversing subsets
                maximum = 0  # Reset maximum and sum values to zero.
                sum = loc[1]
            if not did_it_fit: # location didn't fit in any bin; must create a new subset.
                x = []
                x.append(loc)
                self.subsets.append(x)

    def find_spare_drones(self):
        """
        changes value of self.spare_drones to the appropriate number of spare drones
        needed for proper replacement of all waypoints.
        :return: None
        """
        if self.check_for_single_spare_drone():
            self.subsets = [self.locations]
            self.spare_drones = 1
        else:
            self.create_disjoint_loc_subsets()
            self.spare_drones = len(self.subsets)

    # TODO: Place in checks to see if proper replacement is impossible (i.e. Drones die before first successful replacement)
