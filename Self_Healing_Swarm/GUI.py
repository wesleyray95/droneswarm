from tkinter import *
import math
from Drone import *
from DroneController import *

#Constants
CANVAS_WIDTH = 800
CANVAS_HEIGHT = 800

class GUI:
    """
    GUI that visualizes the position of each drone, their conditions.
    """

    def __init__ (self, master):
        self.width = CANVAS_WIDTH
        self.height = CANVAS_HEIGHT
        self.master = master
        self.master.title("SELF HEALING SWARM GUI")

        self.zone = Canvas(self.master, width=self.width, height=self.height, background="black")
        self.infoBox = Canvas(self.master, width=self.width / 3, height=self.height, background="white")
        # self.droneSelector = Spinbox(self.master, from_=0, to=99999)
        # self.droneSelector.grid(row=0, column=1)
        self.zone.grid(row=0, column=0)
        self.infoBox.grid(row=0, column=1)

        self.img = PhotoImage(file="../images/mapPic.png")

    def drawCircle(self, centerX, centerY, radius=32, borderWidth=10):  # border width not proper
        """
        Draws a circle on the GUI given parameters.
        :param centerX:
        :param centerY:
        :param radius:
        :param borderWidth: width of the border
        :return: none
        """
        leftX = centerX - radius
        rightX = centerX + radius
        topY = centerY + radius
        botY = centerY - radius
        self.zone.create_oval(leftX, topY, rightX, botY, fill="red")

    def drawWayPoint(self, waypoint):
        """
        draws a waypoint as a circle
        :param waypoint:
        :return:
        """
        self.drawCircle(waypoint.xloc, waypoint.yloc, 8)

    def drawHexagon(self, centerX, centerY, radius=32, fillColor='green'):
        """
        Draws a hexagon given a location and radius and fill color.
        :param centerX:
        :param centerY:
        :param radius:
        :param fillColor:
        :return: none
        """
        points = []
        for i in range(6):
            t = (i * math.pi / 3)  # current angle of
            x = centerX + radius * math.cos(t)
            y = centerY + radius * math.sin(t)
            points.append((x, y))

        # print(points)
        # print(points)
        self.zone.create_polygon(points, outline='white', width=1, fill=fillColor)

    def drawDrone(self, drone, controller):
        """
        Draws a drone from it's location, radius, and condition.
        :param drone: A drone object
        :return:
        """
        self.drawHexagon(drone.xloc, drone.yloc, controller.droneRadius, drone.currentBattery.batteryConditionColor())
        self.zone.create_text(drone.xloc, drone.yloc, text=drone.number)

    def drawBackground(self, img):
        """
        Draws the background map.
        :param img: image of the background
        :return:
        """
        self.zone.create_image(self.width / 2, self.height / 2, image=img)

    def drawDroneInfo(self, droneArray):
        """
        Draws drone info on the sidepanel.
        :param droneArray:
        :return:
        """
        self.infoBox.delete('all')
        txt = ""

        for drone in droneArray:
            txt = txt + "Drone Battery " + str(drone.number) + ": "
            bat = '{:.2f}'.format(drone.currentBattery.batteryPercentage)  # formats the battery percentage
            txt = txt + str(bat) + "\n"

        self.infoBox.create_text(80, 100, text=txt)

    def updateGUI(self, controller):
        """
        Updates the GUI based on the drone controller, allows for real time updating of the GUI.
        :param controller: the drone controller
        :return:
        """
        self.zone.delete('all')
        self.drawBackground(self.img)

        for waypoint in controller.waypoints:
            self.drawWayPoint(waypoint)
        for drone in controller.drones:
            self.drawDrone(drone, controller)

        self.drawDroneInfo(controller.drones)

    # def main():
    #     root = Tk()
    #     myGUI = GUI(root)
    #
    #     for x in range(50, 750, 50):
    #         for y in range(50, 750, 50):
    #             myGUI.drawHexagon(x, y, 32)
    #     root.mainloop()
    #
    # if __name__ == "__main__":
    #     main()
