from GUI import *
from tkinter import *
from DroneController import *
from Scheduling import *
from Drone import *

import unittest


class TestController(unittest.TestCase):

    """
    Trivial Test
    """
    def test_trivial(self):
        # trivial test
        self.assertEqual(1, 1)

    # schedule = Scheduling()
    #
    # def test_time_calculation(self):
    #     pass

    def test_spare_drones1(self):
        controller = DroneController(30, 500, 500, 300)
        controller.initializeDroneController()

        # there should be 1 spare drone
        self.assertEqual(1, len(controller.spareDrones))

    def test_spare_drones2(self):
        controller = DroneController(30, 5000, 500, 300)
        controller.initializeDroneController()

        # there should be 2 spare drones
        self.assertEqual(2, len(controller.spareDrones))

    def test_spare_drones5(self):
        controller = DroneController(30, 18000, 500, 300)
        controller.initializeDroneController()

        # there should be 5 spare drones
        self.assertEqual(5, len(controller.spareDrones))

    controller = DroneController(30, 5000, 500, 300)
    controller.initializeDroneController()
    pass

    # root = Tk()
    # myGUI = GUI(root)
    #
    # myGUI.drawBackground(myGUI.img)
    # myGUI.drawDroneInfo(controller.drones)
    #
    # # update GUIs
    # myGUI.updateGUI(controller)
    #
    # # Update GUI
    # root.update()
    # root.update_idletasks()
    #
    # interval = 0.0167  # Delay between updates
    # # Update the GUI and controller at each timer interval (currently every .25 seconds)
    # frame = 0
    # while True:
    #     time.sleep(interval - time.monotonic() % interval)
    #     controller.updateController()
    #     myGUI.updateGUI(controller)
    #     root.update()
    #     root.update_idletasks()
    #     frame += 1


if __name__ == '__main__':
    unittest.main()


