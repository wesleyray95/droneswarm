"Object that controlls all drones and waypoints"

from Drone import *
from Battery import *
from Waypoint import *
from Scheduling import *
import math
import time

class DroneController:
    def __init__(self, radius, area, originX, originY ):
        # Array of all batteries
        self.batteries = []
        # Array of all waypoints
        self.waypoints = []
        # Array of all drones
        self.drones = []
        self.spareDrones = []
        self.locSubsets = []
        # Drones coverage radius
        self.droneRadius = radius
        # Area that we want coverage
        self.coverageArea = area
        # Center of the swarm
        self.swarmOrigin = (originX, originY)
        # Location of drone deployment
        self.droneHQ = Waypoint(5, 5)
        # Keep track of runtime
        self.time = 0
        # Keeps track whether the subset is in a sustain phase.
        self.sustain = []
        # Queue for assigning waypoints to spare drones in their respective subsets
        self.spareQ = []
        # Tells controller when to assign new waypoint to available spare drone.
        self.returned = []
        self.deploy = True


    def initializeDroneController(self):
        """
        Create the drone controller network: necessary drones, waypoints, and batteries
        :return: None
        """
        # Drone Coverage is a hexagon
        droneCoverage = (3 * math.sqrt(3) * math.pow(self.droneRadius, 2)) / 2

        # Initializing a battery and a starting drone
        originWaypoint = Waypoint(self.swarmOrigin[0], self.swarmOrigin[1])
        self.waypoints.append(originWaypoint)

        # We will slowly expand the swarm area until it meets the coverage area of interest to find the number of layers needed
        swarmArea = droneCoverage
        layers = 1
        numDrones = 1

        while (swarmArea < self.coverageArea):
            layers += 1
            numDrones += 6 * (layers - 1)
            swarmArea = numDrones * droneCoverage

        self.initializeWaypoints(layers)
        self.initializeDrones()

        # Creating 20 additional backup batteries
        index = len(self.batteries)
        for i in range(index, index + 20):
            additionalBattery = Battery(100, i)
            self.batteries.append(additionalBattery)

        self.initializeSpareDrones()

    def initializeWaypoints(self, numLayers):
        """
        Create all the waypoints in a hexagonal manner
        :param numLayers: number of layers needed to deploy
        :return: None
        """
        currentLocationX = self.swarmOrigin[0]
        currentLocationY = self.swarmOrigin[1]

        # Creating the waypoints within the Swarm
        theta = 11 * math.pi / 6

        for layer in range(1, numLayers):
            currentLocationY += math.sqrt(3) * self.droneRadius
            # Iterate through each side of the hexagon
            for side in range(6):
                deltaX = math.cos(theta) * math.sqrt(3) * self.droneRadius
                deltaY = math.sin(theta) * math.sqrt(3) * self.droneRadius
                for hexagon in range(layer):
                    currentLocationX += deltaX
                    currentLocationY += deltaY
                    self.waypoints.append(Waypoint(currentLocationX, currentLocationY))
                theta -= math.pi/3

    def initializeDrones(self):
        """
        Loop through all the waypoints and create batteries and drones associated with each one
        :return: None
        """
        counter = 0
        for waypoint in self.waypoints:
            # Create a battery and append it
            battery = Battery(100, counter)
            self.batteries.append(battery)

            # Create a drone and append it
            drone = Drone(self.droneHQ.xloc, self.droneHQ.yloc, True, waypoint, battery)
            drone.heading = drone.assignHeading()
            drone.number = counter
            self.drones.append(drone)

            # Assign the battery to the drone
            battery.assignedDrone = drone

            # Assign the created drones to the waypoint
            waypoint.dronesAssigned.append(drone)

            counter += 1

    def initializeSpareDrones(self):
        test_b = Battery(100, 0)
        test_d = Drone(0, 0, False, Waypoint(0,0), test_b)
        scheduler = Scheduling(self.droneHQ, test_b.L, test_b.drainRate, test_d.speed, self.waypoints, True)
        # Initialize Spare drone array
        for x in range(scheduler.spare_drones):
            battery = Battery(100, 0) # TODO: work on batteryr implementation for spare drones (counter??)
            drone = Drone(self.droneHQ.xloc, self.droneHQ.yloc, False, self.droneHQ, battery)
            drone.number = -1 * (x + 1)
            self.drones.append(drone)
            self.spareDrones.append(drone)
            self.spareQ.append(0)
            self.sustain.append(False)
            self.returned.append(True)
        self.locSubsets = scheduler.subsets
        for x in range(len(self.locSubsets)):
            for y in range(len(self.locSubsets[x])):
                self.locSubsets[x][y] = self.locSubsets[x][y][0]

    def changeBattery(self, drone):
        """
        Changes the current Battery with the next available one, changes the object property itself
        :return: None
        """
        # currentBatteryCharge = drone.currentBattery.batteryPercentage
        # maxBattery = None
        #
        # for battery in self.batteries:
        #     if battery.assignedDrone is None:
        #         # If the battery charge is greater than the current Battery Charge
        #         if battery.batteryPercentage > currentBatteryCharge:
        #             maxBattery = battery
        #         else:
        #             maxBattery = drone.currentBattery
        #
        # drone.currentBattery = maxBattery

        drone.currentBattery.batteryPercentage = 100

    def findAvailableWaypoint(self):
        for waypoint in self.waypoints:
            if len(waypoint.dronesAssigned) == 0:
                return waypoint

        return None

    def updateController(self):
        """
        Update each aspect of the controller
        :return: None
        """

        # for battery in self.batteries:
        #     if battery.assignedDrone is None:
        #         battery.rechargeBattery()
        #     else:
        #         battery.drainBattery()


        for drone in self.drones:
            if drone.deployed:
                # Drain batteries of drones currently deployed
                drone.drainBattery()

        if self.deploy: # Drone Controller is in deploy phase

            for drone in self.drones:  # Update the location of all drones
                drone.updateLocation()

            for i in range(len(self.locSubsets)):  # Loop through all location subsets.

                if self.sustain[i] is False:  # Current subset is in deployment phase.
                    self.spareDrones[i].currentBattery.batteryPercentage = self.spareDrones[i].currentBattery.L
                    allDestReached = True
                    for waypoint in self.locSubsets[i]:
                        if not waypoint.dronesAssigned[0].destReached():
                            allDestReached = False
                    if allDestReached:                      # All waypoint drones reach their destination in subset.
                        self.sustain[i] = True                  # Transition subset to sustain phase.
                        self.spareDrones[i].deployed = True     # deploy spare drones.

                else:                   # Current subset is in sustain phase.
                    if self.returned[i]:    # Check to see if drone has returned.
                        # Assign spare drone to next waypoint in queue from respective subset.
                        self.spareDrones[i].waypoint = self.locSubsets[i][self.spareQ[i]]
                        # Increment queue counter of respective spare drone subset
                        self.spareQ[i] = (self.spareQ[i] + 1) % len(self.locSubsets[i])
                        self.returned[i] = False  # spare drone will now be deployed

                    # Spare drone reaches waypoint.
                    if self.spareDrones[i].destReached():
                        if self.spareDrones[i].waypoint != self.droneHQ:    # Reaches replacement waypoint.
                            # Waypoint drone is temporarily saved for swap.
                            swapDrone = self.spareDrones[i].waypoint.dronesAssigned[0]
                            # Waypoint is assigned replacement drone.
                            self.spareDrones[i].waypoint.dronesAssigned[0] = self.spareDrones[i]
                            # Waypoint drone is assigned droneHQ and placed into spare drone set.
                            swapDrone.waypoint = self.droneHQ
                            self.spareDrones[i] = swapDrone
                        else:       # spare drone reaches droneHQ.
                            print(self.spareDrones[i].currentBattery.batteryPercentage)
                            self.changeBattery(self.spareDrones[i])  # Change spare drone battery.
                            self.returned[i] = True                  # Spare drone can be assigned new waypoint.

        else:   # Drone Controller is in Recall phase.
            pass